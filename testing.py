from main import app
from fastapi.testclient import TestClient

client = TestClient(app)
def test_home():
    print("Test_home")
    response = client.get("/")
    assert response.status_code == 200
    assert "Chatbot" in str(response.content)
    print("Test_home : Berhasil")
    print("-------------------------------")
    # print()

def test_get_bot_response(msg, rply):
    print("test_get_bot_response, Input : "+msg+" , Output : "+rply)
    response = client.get("/get?msg="+msg)
    assert response.status_code == 200
    assert rply in str(response.content)
    print("test_get_bot_response : Berhasil")
    print("-------------------------------")

test_home()
test_get_bot_response("hello", "Hey how are you?")
test_get_bot_response("good evening", "Good Evening")
print("OK")
