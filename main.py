import uvicorn
from pydantic import BaseModel, Field
from fastapi import Request, Depends, BackgroundTasks, Form, File, UploadFile, Header, FastAPI as fastapi
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse

# qst = open("D:/boku no projecto/prosa_ai/problem1/QA_pairs/questions.txt", "r")
# ans = open("D:/boku no projecto/prosa_ai/problem1/QA_pairs/answers.txt", "r")

qst = open("/app/QA_pairs/questions.txt", "r")
ans = open("/app/QA_pairs/answers.txt", "r")

qst_list = qst.readlines()
ans_list = ans.readlines()

for i in range(len(qst_list)) :
    qst_list[i] = qst_list[i][:-1]
    ans_list[i] = ans_list[i][:-1]

def response_ans(msg):
    for i in range(len(ans_list)):
        if (qst_list[i] == msg.lower()):
            return ans_list[i]
    return "I don't know"



app = fastapi()
templates = Jinja2Templates(directory="templates")


@app.get("/")
async def home(request : Request):
    return templates.TemplateResponse("home.php",{
        "request" : request
    })

# UPLOAD dan CONVERSION
@app.get("/get")
async def get_bot_response(request : Request, msg : str):
    return "<Strong>Bot</Strong> : "+ response_ans(msg)
    # return {
    #     "reply" : "<Strong>Bot</Strong> : "+ response_ans(msg)
    # }





if __name__ == "__main__":
    uvicorn.run(app, host = "0.0.0.0", port = 6000)
    # uvicorn.run(app, host = "127.0.0.1", port = 6001)
