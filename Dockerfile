FROM python:3

ADD main.py /
ADD testing.py /

RUN pip install --upgrade pip && \
    pip install uvicorn && \
    pip install pydantic && \
    pip install fastapi && \
    pip install jinja2 && \
    pip install requests

WORKDIR /app

COPY . /app

EXPOSE 6000

CMD [ "python", "./main.py" ]
